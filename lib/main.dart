import 'package:flutter/material.dart';
import 'detail.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Dicoding Flutter Submission'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  String _username = '';
  String _password = '';

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Padding(
        padding: const EdgeInsets.all(30),
        child: ListView(
          children: <Widget>[

            Image.asset('imageAssets/login.png'),

            Text("Login",style: Theme.of(context).textTheme.headline3),
            Text("Please sign in to continue."),
            Container(margin: const EdgeInsets.only(top:20.0),),
            Text("Username",style:Theme.of(context).textTheme.caption),
            Container(margin: const EdgeInsets.only(top:5.0),),
            TextField(onSubmitted: (String value) {setState(() {_username = value;});},
            decoration: InputDecoration(
              border: OutlineInputBorder (
                borderRadius: BorderRadius.circular(10.0),
                borderSide: BorderSide(width: 0,style: BorderStyle.none,)
              ),
              filled: true,
              fillColor: Colors.black12,
            ),),

            Container(margin: const EdgeInsets.only(top:20.0),),
            Text("Password",style:Theme.of(context).textTheme.caption),
            Container(margin: const EdgeInsets.only(top:5.0),),
            TextField(onSubmitted: (String value) {setState(() {_password = value;});},
              decoration: InputDecoration(
                border: OutlineInputBorder (
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(width: 0,style: BorderStyle.none,)
                ),
                filled: true,
                fillColor: Colors.black12,
              ),),

            Container(margin: const EdgeInsets.only(top:20.0),),
            ElevatedButton(
              child: Padding(padding:const EdgeInsets.all(10),child: Text("Log In",style:Theme.of(context).textTheme.bodyMedium)),
              onPressed: ()  {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return const DetailScreen();
                }));
              },
            ),

            Container(margin: const EdgeInsets.only(top:20.0),),
            Text("Forgot Password?"),

          ],
        ),
      ),
    );
  }
}
